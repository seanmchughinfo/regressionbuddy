**RegressionBuddy has moved to GitHub**

RegressionBuddy has multiple contributors now and GitHub provides much better tools for showing off their work.

https://github.com/smchughinfo/regressionbuddy 

-------------------------------------------------
